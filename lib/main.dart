import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'BKK SMK SORE TULUNGAGUNG',
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        title: new Text(
          'Pendaftaran Online',
          style: TextStyle(color: Colors.lightBlue[400]),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            color: Colors.lightBlue[400],
            onPressed: () {},
          ),
        ],
        // leading: Icon(
        //   Icons.chevron_left,
        //   color: Colors.lightBlue[400],
        // ),
        elevation: 1.0,
        centerTitle: true,
        automaticallyImplyLeading: false,
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(1.0),
          child: Container(
            // color: Colors.lightBlue[400],
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                  Colors.blue[300],
                  Colors.green[300],
                  Colors.yellow[300],
                  Colors.orange[300],
                  Colors.red[500],
                  Colors.blue,
                ])),
            padding: EdgeInsets.only(bottom: 3.0),
          ),
        ),
      ),
      drawer: Drawer(),
      // backgroundColor: Colors.blue[300],
      body: GridView.count(
        padding: EdgeInsets.all(5.0),
        crossAxisCount: 2,
        children: <Widget>[
          card(
            logo: "img/jiaec.png",
            nama_perusahaan: "PT JIAEC",
          ),
          card(
            logo: "img/pt-medion.jpg",
            nama_perusahaan: "PT MEDION",
          ),
          card(
            logo: "img/pt-pama.png",
            nama_perusahaan: "PT PAMA",
          ),
          card(
            logo: "img/cipta-futura.png",
            nama_perusahaan: "PT CIPTA FUTURA",
          ),
          card(
            logo: "img/jiaec.png",
            nama_perusahaan: "PT JIAEC",
          ),
          card(
            logo: "img/jiaec.png",
            nama_perusahaan: "PT JIAEC",
          ),
        ],
      ),
    );
  }
}

class card extends StatelessWidget {
  card({this.logo, this.nama_perusahaan});
  String logo;
  String nama_perusahaan;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(3.0),
      child: Card(
        elevation: 2.0,
        child: InkWell(
          onTap: () {},
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Image.asset(
                    logo,
                    fit: BoxFit.fill,
                    // width: MediaQuery.of(context).size.width,
                  ),
                ),
                Text(nama_perusahaan)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
